# Geospatial reference data
## A corporate code list of countries and territories


In order to address the diversity of country and territory codes and names used in the EU institutions, the European Commission has developed a corporate code list of countries and territories that covers the different use cases its services have. This code list was endorsed by the Information Management Steering Board on 27 April 2023 and will be applicable for new IT systems in the Commission from 15 September 2023. Existing IT systems should be aligned if possible. 

The proposed list builds on the already existing [Country authority table](https://op.europa.eu/en/web/eu-vocabularies/dataset/-/resource?uri=http://publications.europa.eu/resource/dataset/country) and provides codes and names of geospatial and geopolitical entities in all official EU languages. It is the result of a combination of multiple relevant standards, created to serve the requirements and use cases (e.g. drop down menus in IT applications, maps, charts, invitation letters) of the EU Institutions services. The list adheres to the principle of one content type per asset and is for this reason limited to current and deprecated countries and territories. This means that it does not include geographical aggregations, such as continents and regions, nor political and/or economic country groupings and organisations. 

The “Corporate list of countries and territories” is **extendable** (e.g. by adding new countries or territories) and can be **enriched** with information that is useful to be maintained centrally (e.g. by adding new properties). These extensions include for example, entities recognized by some UN Member States, such as Kosovo* with the usage of a disclaimer, entities used to express country of citizenship in population and housing censuses legislation, disputed territories (e.g., Abyei Region, Aksai Chin), marine territories (e.g., International Waters) or exception of VAT territories. 

The Data owner role of the corporate code list for country and territories will be performed by Eurostat, whereas the Publications Office of the EU will assume the role of Data steward. Together they will ensure the corporate code list is kept up to date and change requests are properly addressed.  

A draft of the corporate code list of countries and territories is available for your review and evaluation at the following spaces: 

-  documentation and related files are available in this instance  
-  you can browse the content of the corporate code list of countries and territories via the reference data visualisation tool [ShowVoc](https://showvoc.op.europa.eu/#/datasets/Countries_and_territories) 

For comments/feedback concerning this draft version of the corporate code list of countries and territories, please create an issue in this instance. 

If you have any questions, don’t hesitate contacting the [EU Vocabularies support team](mailto:OP-EU-VOCABULARIES@publications.europa.eu).